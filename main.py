from tkinter import *
from tkinter import ttk
from tkinter import messagebox
window = Tk()

tree = ttk.Treeview(window)
window.title("Inventory System")
window.config(background="#e6e6e6")


# command
def add_info():
    product_text = product_entry.get()
    quantity_text = product_quantity_entry.get()
    id_text = product_id_entry.get()
    price_text = price_entry.get()
    if len(product_text) == 0 or len(quantity_text) == 0 or len(id_text) == 0 or len(price_text) == 0:
        messagebox.showinfo(title="Error", message="Please don't leave any fields empty")
    else:
        tree.insert(parent="", index=END, text=product_text, values=(quantity_text, id_text, price_text))


# Label
inventory_title = Label(text="Inventory System", font=("Impact", 30, "normal"), background="#e6e6e6")
inventory_title.grid(column=2, rowspan=5, pady=5, padx=10)
product_name = Label(text="Product: ", font=("Arial", 10, "bold"), background="#e6e6e6")
product_name.grid(row=1, column=0)
product_id = Label(text="Product ID: ", font=("Arial", 10, "bold"), background="#e6e6e6")
product_id.grid(row=2, column=0)
product_quantity = Label(text="Quantity: ", font=("Arial", 10, "bold"), background="#e6e6e6")
product_quantity.grid(row=3, column=0,)
price = Label(text="Price:", font=("Arial", 10, "bold"), background="#e6e6e6")
price.grid(row=4, column=0)
# Entry
product_entry = Entry(width=40)
product_entry.grid(row=1, column=1, padx=5, pady=5)
product_entry.focus()
product_quantity_entry = Entry(width=40)
product_quantity_entry.grid(row=2, column=1, padx=5, pady=5)
product_id_entry = Entry(width=40, )
product_id_entry.grid(row=3, column=1, padx=5, pady=5)
price_entry = Entry(width=40)
price_entry.grid(row=4, column=1, padx=5, pady=5)
delete_entry = Entry(width=30, font=("Arial", 14))
delete_entry.grid(row=13, column=2, columnspan=2)

# button
add_button = Button(text="ADD ITEM", width=30, background="#ff9900", font=("Arial", 10, "normal"), command=add_info)
add_button.grid(row=11, column=1)
deduct_button = Button(text="DEDUCT", width=30, background="#ff6666", font=("Arial", 10, "bold"))
deduct_button.grid(row=13, column=1, padx=10, pady=10)
delete_quantity = Button(text="  DELETE  ", width=45, background="#ff9900", font=("Arial", 10, "normal"))
delete_quantity.grid(row=11, column=2, pady=10, padx=10, columnspan=2)

# style
style = ttk.Style()
column = ("product_id", "quantity", "prices")
tree.configure(columns=column)
tree.heading("#0", text="PRODUCT NAME")
tree.heading(0, text="PRODUCT ID")
tree.heading(1, text="QUANTITY")
tree.heading(2, text="PRICE")
tree.grid(row=6, column=0, columnspan=20, rowspan=5, padx=10, pady=10)
window.mainloop()


